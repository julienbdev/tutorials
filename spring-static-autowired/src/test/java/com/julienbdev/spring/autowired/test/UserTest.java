package com.julienbdev.spring.autowired.test;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.julienbdev.tutorials.spring.autowired.config.SpringConfig;
import com.julienbdev.tutorials.spring.autowired.entity.User;
import com.julienbdev.tutorials.spring.autowired.utils.UserUtils;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class UserTest {

	@Test
	public void isAdult() {
		LocalDate birthday = LocalDate.of(1980, Month.DECEMBER, 25);
		User user = new User("John", birthday);
		Boolean isAdult = UserUtils.isAdult(user);

		Assert.assertTrue(isAdult);
	}
}
