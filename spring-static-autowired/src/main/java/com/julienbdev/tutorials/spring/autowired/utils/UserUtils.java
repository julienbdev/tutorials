package com.julienbdev.tutorials.spring.autowired.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.julienbdev.tutorials.spring.autowired.entity.User;
import com.julienbdev.tutorials.spring.autowired.service.UserService;

@Component
public final class UserUtils {

	private static UserService userService;

	@Autowired
	public UserUtils(UserService userService) {
		UserUtils.userService = userService;
	}

	public static Boolean isAdult(User user) {
		return userService.computeAge(user) > 18;
	}
}
