package com.julienbdev.tutorials.spring.autowired.service;

import java.time.LocalDate;
import java.time.Period;

import org.springframework.stereotype.Service;

import com.julienbdev.tutorials.spring.autowired.entity.User;

@Service
public class UserServiceImpl implements UserService {

	public Integer computeAge(User user) {
		return Period.between(user.getBirthday(), LocalDate.now()).getYears();
	}
}
