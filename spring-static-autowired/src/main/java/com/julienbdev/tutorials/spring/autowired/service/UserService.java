package com.julienbdev.tutorials.spring.autowired.service;

import com.julienbdev.tutorials.spring.autowired.entity.User;

public interface UserService {

	Integer computeAge(User user);
}
